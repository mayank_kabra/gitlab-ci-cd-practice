/* mbed Microcontroller Library
 * Copyright (c) 2023 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbed.h"
#include "src/factorial.h"

#define MAXIMUM_BUFFER_SIZE 32

static UnbufferedSerial pc(USBTX, USBRX); // tx, rx

volatile char inp[MAXIMUM_BUFFER_SIZE] = {0};
volatile int address = 0 ;

char* convertIntegerToChar(int N)
{
 
    // Count digits in number N
    int m = N;
    int digit = 0;
    while (m) {
 
        // Increment number of digits
        digit++;
 
        // Truncate the last
        // digit from the number
        m /= 10;
    }
 
    // Declare char array for result
    char* arr;
 
    // Declare duplicate char array
    char arr1[digit];
 
    // Memory allocation of array
    arr = (char*)malloc(digit);
 
    // Separating integer into digits and
    // accommodate it to character array
    int index = 0;
    while (N) {
 
        // Separate last digit from
        // the number and add ASCII
        // value of character '0' is 48
        arr1[++index] = N % 10 + '0';
 
        // Truncate the last
        // digit from the number
        N /= 10;
    }
 
    // Reverse the array for result
    int i;
    for (i = 0; i < index; i++) {
        arr[i] = arr1[index - i];
    }
 
    // Char array truncate by null
    arr[i] = '\0';
 
    // Return char array
    return (char*)arr;
}


void on_rx_interrupt()
{
    char c ;
    

    char check1[] = "check1\n";
    pc.write(check1, sizeof(check1));

    if(pc.read(&c, sizeof(inp)))
    {   
        char check2[] = "reading inputs\n";
        pc.write(check2, sizeof(check2));

        inp[address] = c ;
        address ++ ;
        // printf("readed input") ; 
        char check3[] = "taken inputs\n";
        pc.write(check3, sizeof(check3));

        pc.write(&c, sizeof(c)) ;

        char check4[] = "printed\n";
        pc.write(check4, sizeof(check4));

        if(c == 't')
        {
            char check5[] = "inside if loop\n";
            pc.write(check5, sizeof(check5));
            
            // printf("taken input") ; 
            int factorial = Factorial::GetFactorial(atoi((const char*)inp));
            char *arr = convertIntegerToChar(factorial);
            for(int i=0; i<sizeof(arr)/sizeof(arr[0]) ; i++)
            {
                pc.write(&arr[i], sizeof(arr[0]));
            }
        }
    }
    
}

int main(int argc, char* argv[])
{

    pc.baud(9600);

    char msg[] = "GGive your input\n";

    pc.write(msg, sizeof(msg));
    
    pc.attach(&on_rx_interrupt, SerialBase::RxIrq);
}


// static DigitalOut led(LED1);

// void on_rx_interrupt()
// {
//     char c;

//     // Toggle the LED.
//     led = !led;

//     // Read the data to clear the receive interrupt.
//     if (serial_port.read(&c, 1)) {
//         // Echo the input back to the terminal.
//         serial_port.write(&c, 1);
//     }
// }

// int main(void)
// {
//     // Set desired properties (9600-8-N-1).
//     serial_port.baud(9600);
//     serial_port.format(
//         /* bits */ 8,
//         /* parity */ SerialBase::None,
//         /* stop bit */ 1
//     );

//     // Register a callback to process a Rx (receive) interrupt.
//     serial_port.attach(&on_rx_interrupt, SerialBase::RxIrq);
// }