# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/mayank/Desktop/gitlab-ci-cd-practice/src/factorial.cpp" "/home/mayank/Desktop/gitlab-ci-cd-practice/build/tst/CMakeFiles/Factorial_test.dir/__/src/factorial.cpp.o"
  "/home/mayank/Desktop/gitlab-ci-cd-practice/tst/test.cpp" "/home/mayank/Desktop/gitlab-ci-cd-practice/build/tst/CMakeFiles/Factorial_test.dir/test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../tst/../src"
  "tst/googletest-src/googletest/include"
  "tst/googletest-src/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/mayank/Desktop/gitlab-ci-cd-practice/build/tst/googletest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/mayank/Desktop/gitlab-ci-cd-practice/build/tst/googletest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
