# CMake generated Testfile for 
# Source directory: /home/mayank/Desktop/gitlab-ci-cd-practice/tst
# Build directory: /home/mayank/Desktop/gitlab-ci-cd-practice/build/tst
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(Factorial_test "/home/mayank/Desktop/gitlab-ci-cd-practice/build/tst/Factorial_test")
set_tests_properties(Factorial_test PROPERTIES  _BACKTRACE_TRIPLES "/home/mayank/Desktop/gitlab-ci-cd-practice/tst/CMakeLists.txt;46;add_test;/home/mayank/Desktop/gitlab-ci-cd-practice/tst/CMakeLists.txt;0;")
subdirs("googletest-build")
